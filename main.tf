terraform {
  backend "http" {
  }

  required_providers {
    heroku = {
      source = "heroku/heroku"
      version = "3.2.0"
    }
  }
}

provider "heroku" {
  api_key = "HEROKU_API_KEY"
  email   = "HEROKU_EMAIL"
}
